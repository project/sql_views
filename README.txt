SQL Views Drupal module
=======================

This is an API module that provides an ability to integrate SQL views in Drupal.

Check out sql_views_examples module for live examples.
